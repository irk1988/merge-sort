/* 
Mergesort version 3 - Data alternates between A and B. Works only for numbers which are power of 2
Author: Immanuel Rajkumar Philip Karunakaran
*/

#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;
bool flag = true;

static void Merge( int src[], int dest[], int p, int q, int r)
{
    int i = p;
    int j = q+1;

    for(int k=p; k<=r; k++ ) {
        if ((j > r) || ((i<=q) && (src[i] <= src[j]) ))
            dest[k] = src[i++];
        else
            dest[k] = src[j++];
    }
}

static int MergeSort( int A[], int B[], int p, int r )
{
    int h1=0, h2=0;
    if(p < r) {
        int q = (p+r)/2;
        h1 = MergeSort(A, B, p, q);
        h2 = MergeSort(A, B, q+1, r);
        if( h1 != h2 ) {
            cout<<"Cannot proceed as the input is not a power of 2\n";
            exit(EXIT_FAILURE);
        } else if( h1 % 2 == 0 ) {
            Merge(A, B, p, q, r);
            flag = false;
        } else {
            Merge(B, A, p, q, r);
            flag = true;
        }
        return h1+1;
    }
    return 0;
}

int main( int argc, char *argv[] )
{

    int n = 0;
    n = atoi(argv[1]); //argument 1 - size of the array


    clock_t start, end;
    double dur = 0;
	double totaltime = 0;

for( int y=0; y<100; y++ ) {

	int *A = new int[n];
    for( int i=0; i<n; i++ )
        A[i] = n-i;

    //Auxilary array for sorting
    int *B = new int[n];
    for( int i=0; i<n; i++ )
        B[i] = A[i];



    start = clock();

    MergeSort(A, B, 0, n-1);

    if( !flag )
        for( int i=0; i<n; i++ )
            A[i] = B[i];

    end = clock();

    for( int j=0; j<n-1; j++ )
        if(A[j] > A[j+1]) {
            cout<<"Sorting Failed !!";
            return 0;
        }
	delete(A);
	delete(B);

    cout<<"\nSorting Success !!\n";
    dur = (double)(end - start)/CLOCKS_PER_SEC*1000;
	totaltime += dur;
}
    cout<<(totaltime/100)<<endl;

    return 0;
}
