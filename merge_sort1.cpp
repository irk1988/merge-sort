/* 
Mergesort version 1 - Allocate dynamic memory for temporary arrays inside merge function
Author: Immanuel Rajkumar Philip Karunakaran
*/

#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

static void Merge( int A[], int p, int q, int r)
{
    int ls = q-p+1;
    int rs = r-q;
    int *L = new int[ls];
    int *R = new int[rs];
    for(int i=p; i<=q; i++) L[i-p] = A[i];
    for(int i=q+1; i<=r; i++) R[i-(q+1)] = A[i];
    int i = 0;
    int j = 0;

    for( int k=p; k<=r; k++ ) {
        if ((j >= rs) || ((i<ls) && (L[i] <= R[j]) ))
            A[k] = L[i++];
        else
            A[k] = R[j++];
    }
    delete(L);
    delete(R);
}

static void MergeSort( int A[], int p, int r )
{
    if(p < r) {
        if(r - p > 11) {
            int q = (p+r)/2;
            MergeSort(A, p, q);
            MergeSort(A, q+1, r);
            Merge(A, p, q, r);
        } else {
            for( int i=p, j=i; i<r; j=++i ) {
                int ai = A[i+1];
                while( ai < A[j] ) {
                    A[j+1] = A[j];
                    if (j-- == p)
                        break;
                }
                A[j+1] = ai;
            }
        }
    }
}

int main( int argc, char *argv[] )
{
    //Input for the program
    int n = 0;
    n = atoi(argv[1]);

    clock_t start, end;

    int *A = new int[n];
    for( int i=0; i<n; i++ )
        A[i] = n-i;

    start = clock();

    MergeSort( A, 0, n-1);

    end = clock();

    for( int j=0; j<n-1; j++ )
        if(A[j] > A[j+1]) {
            cout<<"Sorting Failed !!";
            return 0;
        }

    cout<<"Sorting Success !!\n";
    double dur = (double)(end - start)/CLOCKS_PER_SEC*1000;
    delete(A);
    cout<<"Runtime: "<<dur<<" msec"<<endl;

    return 0;
}
