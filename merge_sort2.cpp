/* 
Mergesort version 2 - Create a single auxiliary array in main and pass it to merge and mergesort functions
Author: Immanuel Rajkumar Philip Karunakaran
*/

#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

static void Merge( int A[], int B[], int p, int q, int r)
{
    for(int i=p; i<=r; i++) B[i] = A[i];
	
    int i = p;
    int j = q+1;

    for(int k=p; k<=r; k++ ) {
        if ((j > r) || ((i<=q) && (B[i] <= B[j]) ))
            A[k] = B[i++];
        else
            A[k] = B[j++];
    }
}

static void MergeSort( int A[], int B[], int p, int r )
{
    if(p < r) {
        if(r - p > 11) {
            int q = (p+r)/2;
            MergeSort(A, B, p, q);
            MergeSort(A, B, q+1, r);
            Merge(A, B, p, q, r);
        } else {
            for( int i=p, j=i; i<r; j=++i ) {
                int ai = A[i+1];
                while( ai < A[j] ) {
                    A[j+1] = A[j];
                    if (j-- == p)
                        break;
                }
                A[j+1] = ai;
            }
        }
    }
}

int main( int argc, char *argv[] )
{

    int n = 0;
    n = atoi(argv[1]); //argument 1 - size of the array

    clock_t start, end;
	double dur = 0;
	double totaltime = 0;

for( int y=0; y<100; y++ ) {
    int *A = new int[n];
    for( int i=0; i<n; i++ )
        A[i] = n-i;

    //Auxilary array for sorting
    int *B = new int[n];


    start = clock();

    MergeSort(A, B, 0, n-1);

    end = clock();

    for( int j=0; j<n-1; j++ )
        if(A[j] > A[j+1]) {
            cout<<"Sorting Failed !!";
            return 0;
        }

    cout<<"\nSorting Success !!\n";
    dur = 0;
	dur = (double)(end - start)/CLOCKS_PER_SEC*1000;
	totaltime +=dur;
	delete(A);
	delete(B);
}
    cout<<(totaltime/100)<<endl;
	return 0;
}
