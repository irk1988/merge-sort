/*
Mergesort version 4 - Data alternates between A and B. Works for all values of n.
Author: Immanuel Rajkumar Philip Karunakaran
*/

#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;
bool flag = true;
int copy1, copy2;

static void Merge( int src[], int dest[], int p, int q, int r)
{
    int i = p;
    int j = q+1;

    for(int k=p; k<=r; k++ ) {
        if ((j > r) || ((i<=q) && (src[i] <= src[j]) ))
            dest[k] = src[i++];
        else
            dest[k] = src[j++];
    }
}

static int MergeSort( int A[], int B[], int p, int r )
{
    int h1=0, h2=0;
    if(p < r) {
        int q = (p+r)/2;
        h1 = MergeSort(A, B, p, q);
        h2 = MergeSort(A, B, q+1, r);
        if( h1 != h2 ) {
            if( flag ) {
                Merge(A, B, p, q, r);
                flag=false;
                for( int y=p; y<=r; y++ )
                    A[y] = B[y];
                copy1++;
            } else {
                Merge(B, A, p, q, r);
                for( int y=p; y<=r; y++ )
                    B[y] = A[y];
                copy2++;
                flag=true;
            }
            return h2+1;
        } else if( h1 % 2 == 0 ) {
            Merge(A, B, p, q, r);
            flag = false;
        } else {
            Merge(B, A, p, q, r);
            flag = true;
        }
        return h1+1;
    }
    return 0;
}

int main( int argc, char *argv[] )
{

    int n = 0;
    n = atoi(argv[1]); //argument 1 - size of the array

    copy1 = copy2 = 0;
    int *A = new int[n];
    for( int i=0; i<n; i++ )
        A[i] = n-i;

    int *B = new int[n];
    for( int i=0; i<n; i++ )
        B[i] = A[i];


    clock_t start, end;
    start = clock();

    MergeSort(A, B, 0, n-1);

    if( !flag )
        for( int i=0; i<n; i++ )
            A[i] = B[i];

    end = clock();

    for( int j=0; j<n-1; j++ ) 
	if(A[j] > A[j+1]) {
            cout<<"Sorting Failed !!";
            return 0;
        }

    cout<<"Sorting Success !!\n";
    double dur = (double)(end - start)/CLOCKS_PER_SEC*1000;
    delete(A);
    delete(B);

    cout<<"Runtime :"<<dur<<" msec"<<endl;
    return 0;

}
